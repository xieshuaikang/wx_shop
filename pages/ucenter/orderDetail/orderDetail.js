var util = require('../../../utils/util.js');
var api = require('../../../config/api.js');

Page({
  data: {
    orderId: 0,
    ordertime:'',
    ordertotal:0,
    orderInfo: {},
    orderGoods: [],
    handleOption: {},
    way:'',
    name:'',
    address:"",
    phone:'',
    storename:"",
  },
  onLoad: function (options) {
    // 页面初始化 options为页面跳转所带来的参数
    console.log(options)
    this.setData({
      orderId: options.id,
      way : options.fidway,
      name:options.fidname,
      address:options.fldaddtess,
      phone:options.fldphone,
      storename : options.fldstorename
    });
    this.getOrderDetail();
  },
  getOrderDetail() {
    let that = this;
    util.request(api.OrderDetail, {fldordernumber: that.data.orderId}).then(function (res) {
      if (res.data.status ==1000) {
          let total = 0;     
          for (var index in res.data.data) {
          
            total = total +res.data.data[index].fldprototal; 

            if (res.data.data[index].fldproductphoto) {
              res.data.data[index].fldproductphoto = api.apiimgurl + res.data.data[index].fldproductphoto.split('|')[1];              
            }
          
         }
        that.setData({
          ordertime:formatTime(res.data.data[0].fldadddate, 'Y/M/D h:m:s'),
          ordertotal:total,
          // orderInfo: res.data.data,
          orderGoods: res.data.data,
          // handleOption: res.data.data
        });
        //that.payTimer();
        console.log(that.data.orderGoods)
      }
    });
  },
 

  payTimer() {
    let that = this;
    let orderInfo = that.data.orderInfo;

    setInterval(() => {
      console.log(orderInfo);
      orderInfo.add_time -= 1;
      that.setData({
        orderInfo: orderInfo,
      });
    }, 1000);
  },
  payOrder() {
    let that = this;
    util.request(api.PayPrepayId, {
      orderId: that.data.orderId || 15
    }).then(function (res) {
      if (res.errno === 0) {
        const payParam = res.data;
        wx.requestPayment({
          'timeStamp': payParam.timeStamp,
          'nonceStr': payParam.nonceStr,
          'package': payParam.package,
          'signType': payParam.signType,
          'paySign': payParam.paySign,
          'success': function (res) {
            console.log(res)
          },
          'fail': function (res) {
            console.log(res)
          }
        });
      }
    });

  },
  onReady: function () {
    // 页面渲染完成
  },
  onShow: function () {
    // 页面显示
  },
  onHide: function () {
    // 页面隐藏
  },
  onUnload: function () {
    // 页面关闭
  }
})

//数据转化  
function formatNumber(n) {
  n = n.toString()
    return n[1] ? n : '0' + n
  
}

function formatTime(number, format) {
    
  var formateArr = ['Y', 'M', 'D', 'h', 'm', 's'];
  var returnArr = [];

  var date = new Date(number * 1);
  returnArr.push(date.getFullYear());
  returnArr.push(formatNumber(date.getMonth() + 1));
  returnArr.push(formatNumber(date.getDate()));

  returnArr.push(formatNumber(date.getHours()));
  returnArr.push(formatNumber(date.getMinutes()));
  returnArr.push(formatNumber(date.getSeconds()));

  for (var i in returnArr) {
    format = format.replace(formateArr[i], returnArr[i]);

  }
  return format;

}