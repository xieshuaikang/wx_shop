var util = require('../../../utils/util.js');
var api = require('../../../config/api.js');
var app = getApp();
Page({
  data: {
    array: [ ],
    index: 0,
    userRegist:'',
    showTopTips: false,
    radioItems: [
      { name: '代理商', value: '0', checked: true },
      { name: '加盟商',value : '1',checked : false},
    ],
    userinfo:{},
    addres: {
      store: '',
      id:0,
      province_id: 0,
      city_id: 0,
      district_id: 0,
      address: '',
      full_region: '',
      name:'',
      mobile: '',
      is_default: 0
    },
    addressId: 0,
    openSelectRegion: false,
    //省市区选择器：
    region: ['请点击选择地址'],
    customItem: '请选择',//为每一列的顶部添加一个自定义的项
    regionType: 1,
    regionList: [],
    selectRegionDone: false
  },

  showTopTips: function () {
    var that = this;
    this.setData({
      showTopTips: true
    });
    setTimeout(function () {
      that.setData({
        showTopTips: false
      });
    }, 3000);
  },
  //省市区选择器：
  bindRegionChange: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      region: e.detail.value
    })
  },

  radioChange: function (e) {
    console.log('radio发生change事件，携带value值为：', e.detail.value);

    var radioItems = this.data.radioItems;
    for (var i = 0, len = radioItems.length; i < len; ++i) {
      radioItems[i].checked = radioItems[i].value == e.detail.value;
    }

    this.setData({
      radioItems: radioItems
    });
  },
  // 地图地址
  chooseLocation() {
    var that = this
    let addres = this.data.addres;
    wx.chooseLocation({
      success: function (res) {
        // console.log(res)
        addres.address = res.address
        that.setData({
          // hasLocation: true,
          // location: formatLocation(res.longitude, res.latitude),
          // locationAddress: res.address
          addres: addres
        })
      }
    })
  },
  bindPickerChange: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      index: e.detail.value
    })
  },
  bindinputMobile(event) {
    let address = this.data.addres;
    address.mobile = event.detail.value;
    this.setData({
      addres: address
    });
  },
  bindinputName(event) {
    console.log(event)
    let address = this.data.addres;
    address.name = event.detail.value.replace(/\s+/g, '');
    this.setData({
      addres: address
    });
  },
  bindinputStore(event) {
    let address = this.data.addres;
    address.store = event.detail.value.replace(/\s+/g, '');
    this.setData({
      addres: address
    });
  },
  bindIsDefault(){
    let address = this.data.addres;
    address.is_default = !address.is_default;
    this.setData({
      addres: address
    });
  },
  // 默认数据展示
  getAddressDetail() {
    let that = this;
    
    util.request(api.findAddress).then(function (res) {
      console.log(res.data.data)
      let useraddress = that.data.addres;
      useraddress.address = res.data.data.fldaddress;
      useraddress.name = res.data.data.flduserdesc;
      useraddress.store = res.data.data.fldshopname;
      let userinfoway = res.data.data.fldtown;
     let address = [];
     address.push(res.data.data.fldprovince);
     address.push(res.data.data.fldcity);
     address.push(res.data.data.fldcounty)
      if (res.data.status == 200) {
        that.setData({
          region : address,
          addres: useraddress,
          index: userinfoway
        });
      } else {
        console.log(' s')

      }
      that.data.userinfo
    });
  },
  setRegionDoneStatus() {
    let that = this;
    let doneStatus = that.data.selectRegionList.every(item => {
      return item.id != 0;
    });

    that.setData({
      selectRegionDone: doneStatus
    })

  },
  onLoad: function (options) {
    var that = this;
    // 页面初始化 options为页面跳转所带来的参数
    
    if (options.id) {
      
      this.setData({
        addressId: options.id
      });
      this.getAddressDetail();
    }
    // 线路信息
    util.request(api.userRegist,'get').then(function (res) {
      let pickarray = [];
      for(var m=0;m<res.data.data.length;m++){
        pickarray.push(res.data.data[m].fldroutename)
      }
      that.setData({
        array: pickarray
      })
    });
  },
  onReady: function () {

  },




  cancelAddress(){
    wx.navigateBack({
      url: '/pages/ucenter/address/address',
    })
  },
  saveAddress(){
    console.log(this.data.addres)
    let address = this.data.addres;

    if (address.name == '') {
      util.showErrorToast('请输入姓名');

      return false;
    }

    if (address.store == '') {
      util.showErrorToast('请输入店铺名称');
      return false;
    }


    if (this.data.region[0] == '') {
      util.showErrorToast('请输入省市区');
      return false;
    }
    if (address.address == '') {
      util.showErrorToast('请输入详细地址');
      return false;
    }


    let that = this;
    util.request(api.addAddress, { 
      flduserdesc: address.name,
      fldprovince: that.data.region[0],//省
      fldcity: that.data.region[1],//市
      fldcounty: that.data.region[2],//县
      fldaddress: address.address,//详细地址
      fldshopname: address.store,//店名
      fldway: parseInt(that.data.index) + 1+'线路',
      fldtown: parseInt(that.data.index),//默认线路用
      is_default: address.is_default//是否设为默认
    }, 'get').then(function (res) {
      if (res.data.status == 200) {
        wx.showToast({
          title: res.data.msg,
          icon: "none",
          duration: 2000,
        });
        wx.navigateBack({
          url: '/pages/ucenter/address/address',
        })
      }else{
        wx.showToast({
          title: res.data.msg,
          icon: "none",
          duration: 2000,
        });
      }
    });

  },
  onShow: function () {
    // 页面显示

  },
  onHide: function () {
    // 页面隐藏

  },
  onUnload: function () {
    // 页面关闭

  }
})