const util = require('../../utils/util.js');
const api = require('../../config/api.js');
const user = require('../../services/user.js');

//获取应用实例
const app = getApp()
Page({
  data: {
    datalist:[ ],//返回的数据
     // 轮播新闻参数
    bchange: 6,//变化值，可自定义设置
    bspeed: 50,//速度，可自定义设置
    direction: "horizontal", //  horizontal=水平 vertical=垂直  ，可设置
    horizontal_dire: "left",// direction: "horizontal" 有效，   left =左    right =右，可设置
    vertical_dire: "bottom",// direction: "vertical" 有效，   top =上    bottom =下，可设置
    strs: [''],//滚动内容，可自定义设置
    clonestr: [],//无缝衔接容器  ,公告
    bw: 0,//容器宽度
    bh: 0,//容器高度
    bl: 0,//容器位置 left
    bt: 0,//容器位置 top
    commodGoods: [],//集团商品
    commodGoodsimg : [],//集团商品图片
    hotGoods: [], //热门商品图片
    brands: [],
    floorGoods: [],
    banner: [], //首页轮播图片
    optionlistimg: [//分类入口图片
      // 'https://f10.ylwxmall.com/image/option/日化类.png',
      // 'https://f10.ylwxmall.com/image/option/食品类.png',
      // 'https://f10.ylwxmall.com/image/option/饮品类.png',
      // 'https://f10.ylwxmall.com/image/option/调味类 .png',
      // 'https://f10.ylwxmall.com/image/option/酒类.png',
      // 'https://f10.ylwxmall.com/image/option/肉类.png',
      // 'https://f10.ylwxmall.com/image/option/炒货.png',
      // 'https://f10.ylwxmall.com/image/option/蔬菜类.png',
      // 'https://f10.ylwxmall.com/image/option/粮油类.png',
      // 'https://f10.ylwxmall.com/image/option/水果类.png',
      'https://f10.ylwxmall.com/image/ceshi/日化类.png',
      'https://f10.ylwxmall.com/image/ceshi/食品类.png',
      'https://f10.ylwxmall.com/image/ceshi/饮品类.png',
      'https://f10.ylwxmall.com/image/ceshi/调味类.png',
      'https://f10.ylwxmall.com/image/ceshi/酒类.png',
      'https://f10.ylwxmall.com/image/ceshi/肉类.png',
      'https://f10.ylwxmall.com/image/ceshi/炒货.png',
      'https://f10.ylwxmall.com/image/ceshi/蔬菜类.png',
      'https://f10.ylwxmall.com/image/ceshi/粮油类.png',
      'https://f10.ylwxmall.com/image/ceshi/水果类.png',
      'https://f10.ylwxmall.com/image/ceshi/vip.png',
    ],
    bannersize:[],//首页轮播及热门商品公用
    optionlist:[],// 商品一级菜单，首页入口文字的遍历
    promotphoto : [],//促销照片
    
  },
  

  onShareAppMessage: function () {
    return {//分享设置
      title: '速运商城',
      desc: '速运商城，分秒必达',
      path: '/pages/index/index'
    }
  },
  // 去掉竖线
sellurl: function(s,k,l) {//值，对象，图片链接
  for (let j = 0, len = s.length; j < len; j++) {
    let photourl = s[j].fldpromotionphoto ? s[j].fldpromotionphoto.split('|').filter(it => it).map(it => l + it.replace('%', '%25')) : ['https://f10.ylwxmall.com/image/AD/noimage.jpg']
    k.push(photourl);
  }
},
sellurltwo: function (s, k, l) {//值，对象，图片链接
  for (let j = 0, len = s.length; j < len; j++) {
    let photourl = s[j].tblproduct.fldphotourl ? s[j].tblproduct.fldphotourl.split('|').filter(it => it).map(it => l + it.replace('%', '%25')) : ['https://f10.ylwxmall.com/image/AD/noimage.jpg']
    k.push(photourl);
  }
},
screen(data) {
  for (var s = 1; s < 5; s++) {
    this.datalist.push(data[data.length - s])
  }
},
  getIndexData: function () {
    let that = this;
    // 轮播接口
    util.request(api.PROMOTIONS).then(function (res) {
      
      let photo = new Array();
      for (var s = 1; s < 5; s++) {
        photo.push(res.data.data[res.data.data.length - s])
      }
      that.setData({
        banner: photo,
      });
    });
    // 水平获取分类
    util.request(api.PRODUCTTYPEONE).then(function (res) {
    
      that.setData({
        optionlist: res.data.data
      });
    });
    // 集团自营商品
    util.request(api.GET_COMPANY_HOT).then(function (res) {
     
      let commodGoodsimgs = [];
      that.sellurltwo(res.data.data, commodGoodsimgs, 'https://f10.ylwxmall.com/image/')
      that.setData({
        commodGoods: res.data.data,
        commodGoodsimg : commodGoodsimgs
      });
      

    });
    // 公告
    util.request(api.GET_NOTICE).then(function (res) {
    let news = new Array();
    for (var s = 0; s < res.data.data.length; s++) {
      news.push(res.data.data[s].flddesc)
    }
      that.setData({
        strs: news
      });
      //无缝滚动函数
      that.seamlessscrolling();
    });
    // 促销查询
    util.request(api.promotquery).then(function (res) {
      let promotqueryphoto = [ ]
      that.sellurltwo(res.data.data, promotqueryphoto, 'https://f10.ylwxmall.com/image/')
      that.setData({
        bannersize: res.data.data,
        promotphoto : promotqueryphoto
      });
    });
  },
  getphoto () {
    let that = this;
    util.request(api.PROMOTIONS).then(function (res) {
      let photo = new Array();
      console.log(res.data.data[0])
      for (let j = 0, len = res.data.data.length; j < len; j++) {
        let photourl = res.data.data[j].tblproduct.fldpromotionphoto.split('|')[1];
        photo.push(photourl);
      }
      that.setData({
        channel: photo,
      });
    })
 
  },
  openToast: function () {
    wx.showToast({
      title: '',
      icon: 'search',
      duration: 3000
    });
  },
 
  switchChange: function (e) {
    
  },
  //无缝滚动函数
  seamlessscrolling: function () {
    var that = this;
    //复制容器
    var clonestr = [];
    for (var i = 0; i < this.data.strs.length; i++) {
      
      clonestr.push(this.data.strs[i]);
    };
    that.setData({ clonestr: clonestr })
    //容器设置 获取设备宽度（可自定义处理）
    wx.getSystemInfo({
      success: function (res) {
        //console.log(res.windowWidth)
        //console.log(res.windowHeight)
        that.setData({ bw: res.windowWidth, bh: 40 })
      }
    })
    //动画
    // var anima = setInterval(function () {
    //   if (that.data.direction == "horizontal") {//水平
    //     if (that.data.bl - that.data.bchange <= -that.data.bw * that.data.strs.length) {
    //       that.setData({ bl: 0 })
    //     } else {
    //       that.setData({ bl: that.data.bl - that.data.bchange })
    //     };
    //   } else if (that.data.direction == "vertical") {//垂直
    //     if (that.data.bt - that.data.bchange <= -that.data.bh * that.data.strs.length) {
    //       that.setData({ bt: 0 })
    //     } else {
    //       that.setData({ bt: that.data.bt - that.data.bchange })
    //     };
    //   };
    // }, that.data.bspeed)

  },
  onLoad: function (options) {
    this.getIndexData();
    // this.switchChange();
    // this.getphoto()
   
   
  },
  onReady: function () {
    
    // 页面渲染完成
  },
  onShow: function () {
    // 页面显示

   
  },
  onHide: function () {
    // 页面隐藏
  },
  onUnload: function () {
    // 页面关闭
  },
});
