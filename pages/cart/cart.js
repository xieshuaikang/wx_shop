var util = require('../../utils/util.js');
var api = require('../../config/api.js');

var app = getApp();

Page({
  data: {
      count:0,                    //总量
      totalMoney:0.00,           // 总价，初始为0
      url:'http://f10.ylwxmall.com:6035/photo/',  //图片路径
      isEditCart: false,
      checkedAllStatus: true,  //是否全选，默认为true
      cart: [],
      gallery: [],
      editCartList: []
  },
  onLoad: function (options) {
    // 页面初始化 options为页面跳转所带来的参数
    // wx.openSetting({
    //   success: (res) => {
    //     console.log('47545')
    //   }
    // })

  },
  onReady: function () {
    // 页渲染完面成

  },
  onShow: function () {
    // 页面显示
      this.getCartList();
  },
  onHide: function () {
    // 页面隐藏

  },
  onUnload: function () {
    // 页面关闭

  },
    /*
    * 一、获取购物车列表
    *
    * */
    getCartList: function () {
      let that = this;
        util.request(api.CartList).then(function (res) {
            let photo = new Array();
            for(let j = 0,len = res.data.data.length; j < len; j++) {
                  let photourl = res.data.data[j].fldphotourl.split('|')[1];
                  photo.push(photourl);
            }
            that.setData({
            cart: res.data.data,
            gallery: photo,
          });
            that.setData({
              checkedAllStatus: false,
              count:0,
              totalMoney:0.00,
            });
      });
    },

    /*
    * 二、单选事件
    *
    * */
    selectList: function (e) {
        let that = this;
        let number = that.data.count;                //选中的商品种类数量
        const index = e.target.dataset.itemIndex;    // 获取data- 传进来的index
        let carts = that.data.cart;                  // 获取购物车列表
        let total = 0.00;                             //合计价格
        const selected = carts[index].checked;       // 获取当前商品的选中状态
        carts[index].checked =!selected;              // 改变状态

        //总价统计
        if (carts[index].checked) {
            total = that.data.totalMoney + carts[index].fldprice*carts[index].fldnumber;
        }
        else {
            total = that.data.totalMoney - carts[index].fldprice*carts[index].fldnumber;
        }

        //选择商品的种类数量
        let num = 0;
        for (let i = 0; i<carts.length; i++){
            if (num == 0){
                number = 0;
            }
            if (carts[i].checked){
               num++;
               number = num;
            }
        }
        if (num !== carts.length){
             that.data.checkedAllStatus = false;
        }else {
             that.data.checkedAllStatus = true;
             number = carts.length;
        }
        that.setData({
            checkedAllStatus: that.data.checkedAllStatus,
            totalMoney: total,
            count:number,
            cart: carts,
        });
    },

    /*
    * 三、全选事件
    *
    * */
    selectAll: function () {
        let that = this;
        let selectAllStatus = that.data.checkedAllStatus;    // 是否全选状态
        let number = that.data.count;                               //选中的商品种类数量
        // let total = that.data.totalMoney;                                       //合计价格
        let carts = that.data.cart;                             // 获取购物车列表
        selectAllStatus = !selectAllStatus;                     // 改变状态
        for (let i = 0; i < carts.length; i++) {
            carts[i].checked = selectAllStatus;                 // 改变所有商品状态
        }
        /*
        * 全选或者全不选
        * 商品种类的数量
        * 商品合计的价格
        */
        if (!selectAllStatus){
            number = 0;
            that.data.totalMoney = 0;
        }else {
            number = carts.length;
            for (let i = 0; i < carts.length; i++) {
                that.data.totalMoney = that.data.totalMoney + carts[i].fldprice*carts[i].fldnumber;
            }
        }
        that.setData({
            checkedAllStatus: selectAllStatus,
            totalMoney: that.data.totalMoney,
            cart: carts,
            count:number,
        });
    },

    /*
    * 四、编辑购物车
    * 1.购物车编辑状态
    * 2.购物车数量 +
    * 3.购物车数量 -
    * 4.购物车持久化
    *
    * */

    //1.购物车编辑状态
    editCart: function () {
        let that = this;
        if (this.data.isEditCart) {
            this.getCartList();
            this.setData({
                isEditCart: !this.data.isEditCart
            });
        } else {
            //编辑状态
            let tmpCartList = this.data.cart.map(function (v) {
                v.checked = false;
                return v;
            });
            this.setData({
                editCartList: this.data.cart,
                cart: tmpCartList,
                isEditCart: !this.data.isEditCart,
                checkedAllStatus: that.selectAll(),
            });
        }

    },

    //2.购物车数量 "+" 逻辑
    addNumber: function (event) {
        let that = this;
        let itemIndex = event.target.dataset.itemIndex;
        let carts = this.data.cart;
        let cartItem = carts[itemIndex];
        let number =  parseInt(cartItem.fldnumber)+1;
        cartItem.fldnumber = number;
        that.setData({
            cart:carts
        });
        that.updateCart(cartItem.fldautoid,cartItem.fldnumber);

    },

    //3.购物车数量 "-" 逻辑
    cutNumber: function (event) {
        let that = this;
        let itemIndex = event.target.dataset.itemIndex;
        let carts = this.data.cart;
        let cartItem = carts[itemIndex];
        let number = (cartItem.fldnumber - 1 > 1) ? cartItem.fldnumber - 1 : 1;
        cartItem.fldnumber = number;
        that.setData({
            cart:carts
        });
        that.updateCart(cartItem.fldautoid,cartItem.fldnumber);
    },

    //4.购物车持久化（购物车id，购买数量：fldautoid,fldnumber）
    updateCart: function (cartId, cartNumber) {
        let that = this;
        util.request(api.CartUpdate, { fldautoid: cartId, fldnumber: cartNumber,}).then(function (res) {
            if (res.errno === 0) {
                console.log(res.data.data);
                that.setData({
                    cart:res.data.cart
                });
            }
        });

    },

    /*
    * 五、购物车删除
    *
    * */
    deleteCart: function () {
        //获取已选择的商品
        let that = this;
        let cartIds = that.data.cart.filter(function (element, index, array) {
            if (element.checked == true) {
                return true;
            } else {
              
                return false;
            }
        });

        if (cartIds.length <= 0) {
          wx.showToast({
            title: '请选择商品',
            icon: 'none',
            duration: 1000
          });
            return false;
        }

        cartIds = cartIds.map(function (element, index, array) {
            if (element.checked == true) {
                return element.fldautoid;
            }
        });

        wx.showModal({
            title: '删除购物车',
            content: '是否确认删除？',
            confirmText: "确认",
            cancelText: "取消",
            success: function (res) {
                if (res.confirm) {
                    util.request(api.CartDelete, { fldautoids: cartIds.toString()}).then(function (re) {
                        that.setData({
                            cart: re.data.data,
                            checkedAllStatus: that.data.checkedAllStatus
                        });
                        that.getCartList()
                    });
                }else{
                    that.getCartList()
                    that.setData({
                        count:0,
                        totalMoney:0,
                        checkedAllStatus: false
                    })
                   
                }
            }
        });

    },

    /*
    * 六、下单
    *
    * */
    checkoutOrder: function () {
        //获取已选择的商品
        let that = this;
        var cartIds = this.data.cart.filter(function (element, index, array) {
            if (element.checked == true) {
                return true;
            } else {
                return false;
            }
        });

        if (cartIds.length <= 0) {
          wx.showToast({
            title: '请选择商品',
            icon: 'none',
            duration: 1000
          });
            return false;
        }

        cartIds = cartIds.map(function (element, index, array) {
            if (element.checked == true) {
                return element.fldautoid;
            }
        });

        util.request(api.OrderSubmit, { fldautoids: cartIds.toString()}).then(function (re) {
            that.setData({
                cart: re.data.data,
                checkedAllStatus: that.data.checkedAllStatus,
            });
            that.getCartList()
            wx.showToast({
              title: re.data.msg,
              icon: 'success',
              duration: 2000,
            });
            if(re.data.status == 1000) {
              wx.navigateTo({
                url:'/pages/ucenter/orderDetail/orderDetail?id='+that.data.cart
              })
            }
        });
    },

})