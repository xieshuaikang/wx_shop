// pages/classify/classify.js
var util = require('../../utils/util.js');
var api = require('../../config/api.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    data : [] ,
    navLeftItems: [],
    navRightItems: [],
    curNav: 19,
    curIndex: 0,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options.userindex)
    var that = this

    util.request(api.producttype).then(function (res) {
      // if (res.errno === 0) {
      for (var index in res.data.data) {
        if (res.data.data[index].fldphotourl) {
          res.data.data[index].fldphotourl = api.apiimgurl + res.data.data[index].fldphotourl.split('|')[1];
        }
      }
      

      that.setData({
        data : res.data.data ,
        navLeftItems: res.data.data.filter(it => it.fldparentid == 0),
        navRightItems: res.data.data.filter(it => it.fldparentid != 0)
        
      })
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    // console.log(this.navLeftItems)
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },

    //事件处理函数
  switchRightTab: function (e) {
    // 获取item项的id，和数组的下标值
    let id = e.target.dataset.id,
      index = parseInt(e.target.dataset.index);
    // 把点击到的某一项，设为当前index
    this.setData({
      curNav: id,
      curIndex: index
    })
  }
})