var util = require('../../utils/util.js');
var api = require('../../config/api.js');

var app = getApp()
Page({
  data: {
    keywrod: '',
    searchStatus: false,
    goodsList: [],
    hotlist:[],
    helpKeyword: [],
    historyKeyword: [],
    categoryFilter: false,
    currentSortType: 'default',
    currentSortOrder: '',
    filterCategory: [],
    defaultKeyword: {},
    hotKeyword: [],
    page: 1,
    size: 20,
    currentSortType: 'id',
    currentSortOrder: 'desc',
    categoryId: 0
  },
  //事件处理函数
  Searchlist: function () {
    console.log(this.data.keyword)

    wx.navigateTo({
      url: "/pages/searchlist/searchlist?keywrod="+this.data.keyword
    })
  },
  clearKeyword: function () {
    this.setData({
      keyword: '',
      searchStatus: false
    });
  },
  onLoad: function () {

    this.getSearchKeyword();
  },

  getSearchKeyword() {
    let that = this;
    util.request(api.getAllGoodsDetailList).then(function (res) {
      if (res.errno !== 0) {
        that.setData({
          // historyKeyword: res.data.historyKeywordList,
          defaultKeyword: res.data.data.list,
          hotlist: res.data.data.list,
          // hotKeyword: res.data.hotKeywordList
        });
      }
    });
  },

  inputChange: function (e) {
    this.setData({
      keyword: e.detail.value,
      searchStatus: false
    });
    this.getHelpKeyword();
  },
  getHelpKeyword: function () {
    let that = this;
    util.request(api.getAllGoodsDetailList, { keyword: that.data.keyword }).then(function (res) {
      console.log(res.data.data.lsit)
       if(res.statusCode==200){
         that.setData({
           helpKeyword: res.data.data.list
         });
       }else{
         
       }
    });
  },
  inputFocus: function () {
    this.setData({
      searchStatus: false,
      goodsList: []
    });

    if (this.data.keyword) {
      this.getHelpKeyword();
    }
  },
  clearHistory: function () {
    this.setData({
      defaultKeyword: []
    })

    // util.request(api.SearchClearHistory, {}, 'POST')
    //   .then(function (res) {
    //     console.log('清除成功');
    //   });
  },
  getGoodsList: function () {
    let that = this;
    util.request(api.GoodsList, { keyword: that.data.keyword}).then(function (res) {
      if (res.errno === 0) {
        that.setData({
          searchStatus: true,
          categoryFilter: false,
          goodsList: res.data.data,
          filterCategory: res.data.filterCategory,
          page: res.data.currentPage,
          size: res.data.numsPerPage
        });
      }

      //重新获取关键词
      that.getSearchKeyword();
    });
  },
  onKeywordTap: function (event) {
    // enter
    // console.log('fds')
    this.getSearchResult(event.target.dataset.keyword);

  },
   getSearchResult(keyword) {

    this.setData({
      keyword: keyword,
      page: 1,
      categoryId: 0,
      goodsList: []
    });

    this.getGoodsList();
  },
  // openSortFilter: function (event) {
  //   let currentId = event.currentTarget.id;
  //   switch (currentId) {
  //     case 'categoryFilter':
  //       this.setData({
  //         'categoryFilter': !this.data.categoryFilter,
  //         'currentSortOrder': 'asc'
  //       });
  //       break;
  //     case 'priceSort':
  //       let tmpSortOrder = 'asc';
  //       if (this.data.currentSortOrder == 'asc') {
  //         tmpSortOrder = 'desc';
  //       }
  //       this.setData({
  //         'currentSortType': 'price',
  //         'currentSortOrder': tmpSortOrder,
  //         'categoryFilter': false
  //       });

  //       // this.getGoodsList();
  //       break;
  //     default:
  //       //综合排序
  //       this.setData({
  //         'currentSortType': 'default',
  //         'currentSortOrder': 'desc',
  //         'categoryFilter': false
  //       });
  //       // this.getGoodsList();
  //   }
  // },
  selectCategory: function (event) {
    let currentIndex = event.target.dataset.categoryIndex;
    let filterCategory = this.data.filterCategory;
    let currentCategory = null;
    for (let key in filterCategory) {
      if (key == currentIndex) {
        filterCategory[key].selected = true;
        currentCategory = filterCategory[key];
      } else {
        filterCategory[key].selected = false;
      }
    }
    this.setData({
      'filterCategory': filterCategory,
      'categoryFilter': false,
      categoryId: currentCategory.id,
      page: 1,
      goodsList: []
    });
    // this.getGoodsList();
  },
  onKeywordConfirm(event) {
    this.Searchlist()
    this.getSearchResult(event.detail.value);
  }
})