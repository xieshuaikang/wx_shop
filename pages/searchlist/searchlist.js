var util = require('../../utils/util.js');
var api = require('../../config/api.js');



var app = getApp();

Page({
  data: {
    scrollLeft: 0,
    scrollTop: 0,
    // scrollHeight: 0,
    keyword : '',
    goodslist : [],
    tip:false

  },
  getlistKeyword: function () {
    let that = this;
    util.request(api.getAllGoodsDetailList, { keyword: this.data.keyword }, 'post').then(function (res) {
      console.log(res)
      if (res.status == 200) {
        that.setData({
          goodslist: res.data.data.list
        });
      }else {
        that.setData({
          tip: true
        });
      }
    });
  },
  onLoad: function (e) {
    let that = this;
    util.request(api.getAllGoodsDetailList, { keyword: e.keywrod }, 'get').then(function (res) {
      for (var index in res.data.data.list) {
        if (res.data.data.list[index].fldphotourl) {
          res.data.data.list[index].fldphotourl = api.apiimgurl + res.data.data.list[index].fldphotourl.split('|')[1];
        }
      }
      console.log(res)
      if (res.statusCode == 200) {
        that.setData({
          keyword: e.keywrod,
          goodslist: res.data.data.list
        });
      } else {
        console.log('this.data.goodslist')
      }
      console.log(that.data.goodslist)
    });
    
    // console.log(e.keywrod)
  },
  onReady: function () {
    // console.log(this.data.goodslist)
  },
  onShow: function () {

  },
  onHide: function () {
    // 页面隐藏

  },
  onUnload: function () {
    // 页面关闭
  }
})