var app = getApp();
var WxParse = require('../../lib/wxParse/wxParse.js');
var util = require('../../utils/util.js');
var api = require('../../config/api.js');
const Zan = require('../../dist/index');
Page({
  data: {
      url:'https://f10.ylwxmall.com/image/',
      id: 0,
      typeid:0,
      fldmemo:'',
      productListItems:[],//大家都买
      product: [],
      gallery: [],
      cart:[],
      // specificationList: [],
      // productList: [],
      // relatedProduct: [],   //相关产品
      cartProductNumber: 0,  //购物车内商品数量
      // userHasCollect: 0,    //商品收藏
      number: 1,
      checkedSpecText: '请选择规格数量',
      openAttr: false,
      showBottomPopup :false ,//控制弹窗
      noCollectImage: "/static/images/icon_collect.png",
      hasCollectImage: "/static/images/icon_collect_checked.png",
      collectBackImage: "/static/images/icon_collect.png",
      min: 1,//最小值 整数类型，null表示不设置
      max: null,//最大值 整数类型，null表示不设置
      num: 1,//输入框数量 整数类型
      change: 1,//加减变化量 整数类型
      def_num: 0//输入框值出现异常默认设置值

},
  //输入框失去焦点事件
  evblur: function (e) {
    var zval = parseInt(e.detail.value);
    console.log(zval)
    //正则 正整数 0 负整数
    if (/(^-[1-9][0-9]{0,}$)|(^0$)|(^[1-9][0-9]{0,}$)/.test(zval)) {
      //最大值
      if (this.data.max != null) {
        if (zval > this.data.max) {
          wx.showToast({
            title: '超出最大值',
            icon: 'none',
            duration: 1000
          });
          this.setData({ num: this.data.def_num })
        } else {
          this.setData({ num: zval })
        };
      } else {
        this.setData({ num: zval })
      };
      //最小值
      if (this.data.min != null) {
        if (zval < this.data.min) {
          wx.showToast({
            title: '低于最小值',
            icon: 'none',
            duration: 1000
          });
          this.setData({ num: this.data.def_num })
        } else {
          this.setData({ num: zval })
        };
      } else {
        this.setData({ num: zval })
      };
    } else {
      wx.showToast({
        title: '不是整数',
        icon: 'none',
        duration: 1000
      });
      this.setData({ num: this.data.def_num })
    };
  },
  //加
  evad: function () {
    var cval = Number(this.data.num) + this.data.change;
    if (this.data.max != null) {
      if (cval > this.data.max) {
        wx.showToast({
          title: '超出最大值',
          icon: 'none',
          duration: 1000
        });
      } else {
        this.setData({ num: cval })
      };
    } else {
      this.setData({ num: cval })
    };
  },
  //减
  evic: function () {
    var cval = Number(this.data.num) - this.data.change;
    if (this.data.min != null) {
      if (cval < this.data.min) {
       
        wx.showToast({
          title: '低于最低值',
          icon: 'none',
          duration: 1000
        });
      } else {
        this.setData({ num: cval })
      };
    } else {
      this.setData({ num: cval })
    };
  },

  toggleBottomPopup() {
    this.setData({
      showBottomPopup: !this.data.showBottomPopup
    });
  },
  
    /*查看商品详情*/
    getProductInfo: function () {
    let that = this;
        util.request(api.productDetail, { fldAutoID: that.data.id }).then(function (res) {
       // if (res.errno === 0) {
          let gallerys = [];
          res.data.data.fldphotourl.split('|').forEach(function (item, i, val) {
                gallerys.push(item)
          });
          if (gallerys.length>1){
              gallerys.shift();
          }
          that.setData({
            product: res.data.data,
            gallery: gallerys,
            max:res.data.data.fldnumbers
          });
          console.log(that.data.gallery)
         
          // for (n = 0; n < that.data.gallery.length;n++) {
          //   gallerys.push()
          // }
        if (res.data.userHasCollect == 1) {
          that.setData({
            'collectBackImage': that.data.hasCollectImage
          });
        } else {
          that.setData({
            'collectBackImage': that.data.noCollectImage
          });
        }
        // that.getProductRelated();
     // }
    });
        // 大家都在看
        util.request(api.producttypelist, { fldAutoID: that.data.typeid }, ).then(function (res) {
          console.log(res.data)
          for (var index in res.data.data.list) {
            
            if (res.data.data.list[index].fldphotourl) {
              res.data.data.list[index].fldphotourl = api.apiimgurl + res.data.data.list[index].fldphotourl.split('|')[1];
            }
          }

          that.setData({
            productListItems: res.data.data.list
          })
        })

  },

    /*获取相关商品*/
    getProductRelated: function () {
      let that = this;
      util.request(api.productRelated, { id: that.data.id }).then(function (res) {
        console.log(res.data)
        if (res.errno === 0) {
          that.setData({
              productRelated: res.data.productList,
          });
        }
      });

    },
  clickSkuValue: function (event) {
    let that = this;
    let specNameId = event.currentTarget.dataset.nameId;
    let specValueId = event.currentTarget.dataset.valueId;

    //判断是否可以点击

    //TODO 性能优化，可在wx:for中添加index，可以直接获取点击的属性名和属性值，不用循环
    let _specificationList = this.data.specificationList;
    for (let i = 0; i < _specificationList.length; i++) {
      if (_specificationList[i].specification_id == specNameId) {
        for (let j = 0; j < _specificationList[i].valueList.length; j++) {
          if (_specificationList[i].valueList[j].id == specValueId) {
            //如果已经选中，则反选
            if (_specificationList[i].valueList[j].checked) {
              _specificationList[i].valueList[j].checked = false;
            } else {
              _specificationList[i].valueList[j].checked = true;
            }
          } else {
            _specificationList[i].valueList[j].checked = false;
          }
        }
      }
    }
    this.setData({
      'specificationList': _specificationList
    });
    //重新计算spec改变后的信息
    this.changeSpecInfo();

    //重新计算哪些值不可以点击
  },

  //获取选中的规格信息
  getCheckedSpecValue: function () {
    let checkedValues = [];
    let _specificationList = this.data.specificationList;
    for (let i = 0; i < _specificationList.length; i++) {
      let _checkedObj = {
        nameId: _specificationList[i].specification_id,
        valueId: 0,
        valueText: ''
      };
      for (let j = 0; j < _specificationList[i].valueList.length; j++) {
        if (_specificationList[i].valueList[j].checked) {
          _checkedObj.valueId = _specificationList[i].valueList[j].id;
          _checkedObj.valueText = _specificationList[i].valueList[j].value;
        }
      }
      checkedValues.push(_checkedObj);
    }

    return checkedValues;

  },
  //根据已选的值，计算其它值的状态
  setSpecValueStatus: function () {

  },
  //判断规格是否选择完整
  isCheckedAllSpec: function () {
    return !this.getCheckedSpecValue().some(function (v) {
      if (v.valueId == 0) {
        return true;
      }
    });
  },
  // getCheckedSpecKey: function () {
  //   let checkedValue = this.getCheckedSpecValue().map(function (v) {
  //     return v.valueId;
  //   });

  //   return checkedValue.join('_');
  // },
  changeSpecInfo: function () {
    let checkedNameValue = this.getCheckedSpecValue();

    //设置选择的信息
    let checkedValue = checkedNameValue.filter(function (v) {
      if (v.valueId != 0) {
        return true;
      } else {
        return false;
      }
    }).map(function (v) {
      return v.valueText;
    });
    if (checkedValue.length > 0) {
      this.setData({
        'checkedSpecText': checkedValue.join('　')
      });
    } else {
      this.setData({
        'checkedSpecText': '请选择规格数量'
      });
    }

  },
  // getCheckedProductItem: function (key) {
  //   return this.data.productList.filter(function (v) {
  //     if (v.goods_specification_ids == key) {
  //       return true;
  //     } else {
  //       return false;
  //     }
  //   });
  // },
  onLoad: function (options) {
      // 页面初始化 options为页面跳转所带来的参数
    this.setData({
      id: parseInt(options.fldautoid),
      typeid: parseInt(options.fldtypeid),
      fldmemo: options.fldmemo
      // id: 1181000
    });
      // util.request(api.Login);
      let that = this;
      that.getProductInfo();
  },

  onReady: function () {
    // 页面渲染完成

  },
  onShow: function () {
    // 页面显示

  },
  onHide: function () {
    // 页面隐藏

  },
  onUnload: function () {
    // 页面关闭

  },
  switchAttrPop: function () {
    if (this.data.openAttr == false) {
      this.setData({
        openAttr: !this.data.openAttr,
        collectBackImage: "/static/images/detail_back.png"
      });
    }
  },
  closeAttrOrCollect: function () {
    let that = this;
    if (this.data.openAttr) {
      this.setData({
        openAttr: false,
      });
      if (that.data.userHasCollect == 1) {
        that.setData({
          'collectBackImage': that.data.hasCollectImage
        });
      } else {
        that.setData({
          'collectBackImage': that.data.noCollectImage
        });
      }
    } else {
      //添加或是取消收藏
      util.request(api.CollectAddOrDelete, { typeId: 0, valueId: this.data.id }, "POST")
        .then(function (res) {
          console.log(res.data)
          let _res = res;
          if (_res.errno == 0) {
            if ( _res.data.type == 'add') {
              that.setData({
                'collectBackImage': that.data.hasCollectImage
              });
            } else {
              that.setData({
                'collectBackImage': that.data.noCollectImage
              });
            }

          } else {
            wx.showToast({
              image: '/static/images/icon_error.png',
              title: _res.errmsg,
              mask: true
            });
          }

        });
    }

  },
  openCartPage: function () {
    wx.switchTab({
      url: '/pages/cart/cart',
    });
  },
   toHome: function (){
       wx.switchTab({
           url: '/pages/index/index',
       });
    },
  addToCart: function () {
    var that = this;
    // if (this.data.openAttr == false) {
    //   //打开规格选择窗口
    //   this.setData({
    //     openAttr: !this.data.openAttr,
    //     collectBackImage: "/static/images/detail_back.png"
    //   });
    // } 
    // else 

      //提示选择完整规格
      // if (!this.isCheckedAllSpec()) {
      //   return false;
      // }

      //根据选中的规格，判断是否有对应的sku信息
      // let checkedProduct = this.getCheckedProductItem(this.getCheckedSpecKey());
      // if (!checkedProduct || checkedProduct.length <= 0) {
      //   //找不到对应的product信息，提示没有库存
      //   return false;
      // }

      // //验证库存
      // if (checkedProduct.goods_number < this.data.number) {
      //   //找不到对应的product信息，提示没有库存
      //   return false;
      // }
    that.toggleBottomPopup()
      //添加到购物车
      util.request(api.CartAdd, {fldproductid:that.data.product.fldautoid, fldproductname:that.data.product.fldname, fldcolor:that.data.product.fldcolor,
                                    fldsize:that.data.product.fldsize, fldprice:that.data.product.fldprice,  fldphotourl:that.data.product.fldphotourl,
                                    fldnumber: that.data.num})
        .then(function (res) {
         console.log(res)
            wx.showToast({
              title: '添加成功',
              icon: 'none',
              duration: 500
            });
            that.setData({
              // openAttr: !that.data.openAttr,
              cartProductCount: that.data.num,
            });
            if (that.data.userHasCollect == 1) {
              that.setData({
                'collectBackImage': that.data.hasCollectImage
              });
            } else {
              that.setData({
                'collectBackImage': that.data.noCollectImage
              });
            }
          // } else {
          //   wx.showToast({
          //     image: '/static/images/icon_error.png',
          //     title: _res.errmsg,
          //     mask: true
          //   });
          // }

        });
    

  },
})