var api = require('../../../config/api.js');
var util = require('../../../utils/util.js');
var app = getApp();
Page({
  data: {
    username: '',
    code: '',
  },
  onLoad: function (options) {
    // 页面初始化 options为页面跳转所带来的参数
    // 页面渲染完成
    
  },
  onReady: function () {

  },
  onShow: function () {
    // 页面显示

  },
  onHide: function () {
    // 页面隐藏

  },
  onUnload: function () {
    // 页面关闭

  },
    //发送验证码
    sendSms(){
        var session_id = wx.getStorageSync('sessionid');//本地取存储的sessionid
        if (session_id != "" && session_id != null) {
        }

        const username = this.data.username
        if(username.length!=11){
            wx.showModal({
                title: '错误信息',
                content: '手机号码填写有误',
                showCancel: false
            });
            return;
        }
        wx.request({
            url:api.sendSms,
            data:{
                'fldusername': username
            },
            header : {
                'content-type': 'application/json',
                'Cookie': 'JSESSIONID=' + session_id
            },
            success(res){
              wx.showToast({
                title: res.data.msg,
                icon: 'success',
                duration: 500
              });
                // if (res.data.data.status == 200) {
                // }
            }
        })

    },
  //
  // codeCheck:function () {
  //   let that = this;
  //   let code = that.data.code;
  //   let codeStatus = that.data.codeStatus;
  //   if (code.length!=6){
  //       util.showErrorToast('验证码格式错误')
  //       return;
  //   }
  //     util.request(api.codeCheck,{code:code}).then(function (res) {
  //         if (res.data.status == 200){
  //             that.setData({
  //                 codeStatus:1
  //             })
  //             util.showErrorToast(res.data.msg);
  //             return;
  //         }
  //         util.showErrorToast(res.data.msg);
  //     })
  //
  // },



  startLogin: function(){
    var that = this;
    let code = that.data.code;
    // var codeStatus = that.data.codeStatus;
    // if (codeStatus === 0){
    //     wx.showModal({
    //         title: '错误信息',
    //         content: '请先验证验证码',
    //         showCancel: false
    //     });
    //     return;
    // }
    util.request(api.register,{code:code}).then(function (res) {
        if (res.data.status == 200) {
            util.showErrorToast(res.data.msg);
            wx.redirectTo({
              url: '/pages/index/index',
              success: function(res) {},
              fail: function(res) {},
              complete: function(res) {},
            })
        }else if (res.data.status == 500){
            util.showErrorToast(res.data.msg);
        }else if (res.data.status == 501){
            util.showErrorToast(res.data.msg);
        }else if (res.data.status == 502){
            util.showErrorToast(res.data.msg);
        }else if (res.data.status == 503){
            util.showErrorToast(res.data.msg);
        }
    })






  },
    //将输入框的内容赋给 username
  bindUsernameInput: function(e){
    this.setData({
      username: e.detail.value
    });
  },
    //将输入框的内容赋给 code
  bindCodeInput: function(e){
    this.setData({
      code: e.detail.value
    });
  },



  //清空输入框内输入的内容
  clearInput: function(e){
    switch (e.currentTarget.id){
      case 'clear-username':
        this.setData({
          username: ''
        });
        break;
        case 'clear-code':
        this.setData({
          code: ''
        });
        break;
    }
  }
})