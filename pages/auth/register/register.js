var util = require('../../../utils/util.js');
var api = require('../../../config/api.js');
var md5 = require('../../../utils/md5.js')
var app = getApp();
Page({
  data: {
    showTopTips: false,
    radioItems: [
      { name: '代理商', value: '0' },
      { name: '加盟商', value: '1', checked: true }
    ],
    username: '',
    password: '',
    confirmPassword: '',
    code: '',
    loginErrorCount: 0,
    address: { },
    addres: {
      id: 0,
      province_id: 0,
      city_id: 0,
      district_id: 0,
      address: '',
      full_region: '',
      name: '',
      mobile: '',
      is_default: 0
    },
    userInfo: {
      "MobileNo": "",
      "Sex": "0",
      "UserName": "",
      "store_name": "",
      'password': '',
    },
    store_name: '',
    town_name: '',
    addressId: 0,
    openSelectRegion: false,
    selectRegionList: [
      { id: 0, name: '省份', parent_id: 1, type: 1 },
      { id: 0, name: '城市', parent_id: 1, type: 2 },
      { id: 0, name: '区县', parent_id: 1, type: 3 }
    ],
    regionType: 1,
    regionList: [],
    selectRegionDone: false
  },
  showTopTips: function () {
    var that = this;
    this.setData({
      showTopTips: true
    });
    setTimeout(function () {
      that.setData({
        showTopTips: false
      });
    }, 3000);
  },
  seleadder: function () {
    wx.navigateTo({
      url: '/lib/citySelector/switchcity/switchcity',
    });
  },
   
    radioChange: function (e) {
    console.log('radio发生change事件，携带value值为：', e.detail.value);

    var radioItems = this.data.radioItems;
    for (var i = 0, len = radioItems.length; i < len; ++i) {
      radioItems[i].checked = radioItems[i].value == e.detail.value;
    }

    this.setData({
      radioItems: radioItems
    });
  },

  onReady: function () {

  },
  onShow: function () {
    // 页面显示
    console.log(this.data.address)

  },
  onHide: function () {
    // 页面隐藏

  },
  onUnload: function () {
    // 页面关闭

  },
  sendSms() {
    var session_id = wx.getStorageSync('sessionid');//本地取存储的sessionid
    if (session_id != "" && session_id != null) {

    }
    const mobile = this.data.address.mobile
    wx.request({
      url: api.sendSms,
      data: {
        'fldusername': mobile
      },
      header: {
        'content-type': 'application/json',
        'Cookie': 'JSESSIONID=' + session_id
      },
      success(res) {
        util.showErrorToast(res.data.msg);
        // if (res.data.data.status == 200) {
        // }
      }
    })
  },
  startRegister: function () {
    var that = this;

    if (that.data.password.length < 3 || that.data.address.mobile.length < 3) {
      wx.showModal({
        title: '错误信息',
        content: '用户名和密码不得少于3位',
        showCancel: false
      });
      return false;
    }

    if (that.data.password != that.data.confirmPassword) {
      wx.showModal({
        title: '错误信息',
        content: '确认密码不一致',
        showCancel: false
      });
      return false;
    }

    let address = this.data.address;

    if (address.name == '') {
      util.showErrorToast('请输入姓名');

      return false;
    }

    if (address.mobile == '') {
      util.showErrorToast('请输入手机号码');
      return false;
    }


    if (address.district_id == 0) {
      util.showErrorToast('请输入省市区');
      return false;
    }

    if (address.address == '') {
      util.showErrorToast('请输入详细地址');
      return false;
    }

    var session_id = wx.getStorageSync('sessionid');//本地取存储的sessionid
    wx.request({
      url: api.register,
      data: {
        fldusername: address.mobile,
        'fldpassword': md5.hex_md5(that.data.password),
        // 'fldusername': this.newPhoneNumber,
        'code': this.data.code,
        'flduserdesc': address.name,
        // 'fldpassword':this.$md5(this.userInfo.password),
        // 'fldtype': this.userInfo.Sex,
        'fldshopname': this.data.store_name,
        // 'flddistrict': this.distinctAddress,
        'fldprovince': address.province_name,
        'fldcity': address.city_name,
        'fldcounty': address.district_name, //county为县,与country(乡村)不同
        'fldtown': this.data.town_name,
        // 'village':this.village,
        'fldaddress': address.address,
        // 'fldway':this.deliverRoute.fldroutename
        // id: address.id,
        // name: address.name,
        // mobile: address.mobile,
        // is_default: address.is_default,
      },
      method: 'POST',
      header: {
        'content-type': 'application/x-www-form-urlencoded',
        'Cookie': 'JSESSIONID=' + session_id
      },
      success: function (res) {
        wx.showToast({
          // title: '注册成功',
          title: res.data.msg,
          duration: 2000,
          complete: function () {
            // wx.navigateBack({
            //     delta: 1
            // });
            if (res.data.status == 200) {
              wx.navigateTo({
                url: '/pages/index/index'
              })
            }

          }
        })
        // if (res.data.status == 200) {
        //
        // } else {
        //     wx.showToast({
        //         title:'提示',
        //     })
        // }
      }
    });
  },
  bindUsernameInput: function (e) {

    this.setData({
      username: e.detail.value
    });
  },
  bindPasswordInput: function (e) {

    this.setData({
      password: e.detail.value
    });
  },
  bindConfirmPasswordInput: function (e) {

    this.setData({
      confirmPassword: e.detail.value
    });
  },
  bindCodeInput: function (e) {

    this.setData({
      code: e.detail.value
    });
  },
  bindinputShopname: function (e) {
    this.setData({
      store_name: e.detail.value
    })
  },
  bindinputTownname(e) {
    // this.setData({
    //     town_name: e.detail.value
    // })
    let address = this.data.address;
    wx.chooseAddress({
      success: (res) => {
        address.province_name = res.provinceName
        address.city_name = res.cityName
        address.district_name = res.countyName
        address.full_region = [res.provinceName, res.cityName, res.countyName].join('-')
        address.mobile = res.telNumber
        address.name = res.userName
        address.address = res.detailInfo
        this.setData({
          username: res.userName,
          address: address,
        });
        // console.log(res)
        // console.log(res.userName)
        // console.log(res.postalCode)
        // console.log(res.provinceName)
        // console.log(res.cityName)
        // console.log(res.countyName)
        // console.log(res.detailInfo)
        // console.log(res.nationalCode)
        // console.log(res.telNumber)
      }
    })
  },
  clearInput: function (e) {
    switch (e.currentTarget.id) {
      case 'clear-username':
        this.setData({
          username: ''
        });
        break;
      case 'clear-password':
        this.setData({
          password: ''
        });
        break;
      case 'clear-confirm-password':
        this.setData({
          confirmPassword: ''
        });
        break;
      case 'clear-code':
        this.setData({
          code: ''
        });
        break;
    }
  },

  bindinputMobile(event) {
    let address = this.data.address;
    address.mobile = event.detail.value;
    this.setData({
      address: address
    });
  },
  bindinputName(event) {
    let address = this.data.address;
    address.name = event.detail.value;
    this.setData({
      address: address
    });
  },
  bindinputAddress(event) {
    let address = this.data.address;
    address.address = event.detail.value;
    this.setData({
      address: address
    });
  },
  chooseLocation() {
    var that = this
    let address = this.data.address;
    wx.chooseLocation({
      success: function (res) {
        // console.log(res)
        address.address = res.address
        console.log(res)
        that.setData({
          // hasLocation: true,
          // location: formatLocation(res.longitude, res.latitude),
          // locationAddress: res.address
          address: address
        })
      }
    })
  },
  bindIsDefault() {
    let address = this.data.address;
    address.is_default = !address.is_default;
    this.setData({
      address: address
    });
  },
  getAddressDetail() {
    let that = this;
    util.request(api.AddressDetail, { id: that.data.addressId }).then(function (res) {
      if (res.errno === 0) {
        that.setData({
          address: res.data
        });
      }
    });
  },
  setRegionDoneStatus() {
    let that = this;
    let doneStatus = that.data.selectRegionList.every(item => {
      return item.id != 0;
    });

    that.setData({
      selectRegionDone: doneStatus
    })

  },
  chooseRegion() {
    let that = this;
    this.setData({
      openSelectRegion: !this.data.openSelectRegion
    });

    //设置区域选择数据
    let address = this.data.address;
    if (address.province_id > 0 && address.city_id > 0 && address.district_id > 0) {
      let selectRegionList = this.data.selectRegionList;
      selectRegionList[0].id = address.province_id;
      selectRegionList[0].name = address.province_name;
      selectRegionList[0].parent_id = 1;

      selectRegionList[1].id = address.city_id;
      selectRegionList[1].name = address.city_name;
      selectRegionList[1].parent_id = address.province_id;

      selectRegionList[2].id = address.district_id;
      selectRegionList[2].name = address.district_name;
      selectRegionList[2].parent_id = address.city_id;

      this.setData({
        selectRegionList: selectRegionList,
        regionType: 3
      });

      this.getRegionList(address.city_id);
    } else {
      this.setData({
        selectRegionList: [
          { id: 0, name: '省份', parent_id: 1, type: 1 },
          { id: 0, name: '城市', parent_id: 1, type: 2 },
          { id: 0, name: '区县', parent_id: 1, type: 3 }
        ],
        regionType: 1
      })
      this.getRegionList(1);
    }

    this.setRegionDoneStatus();

  },
  onLoad: function (options) {
    // 页面初始化 options为页面跳转所带来的参数
    console.log(options)
    if (options.id) {
      this.setData({
        addressId: options.id
      });
      this.getAddressDetail();
    }

    this.getRegionList(1);

  },
  selectRegionType(event) {
    let that = this;
    let regionTypeIndex = event.target.dataset.regionTypeIndex;
    let selectRegionList = that.data.selectRegionList;

    //判断是否可点击
    if (regionTypeIndex + 1 == this.data.regionType || (regionTypeIndex - 1 >= 0 && selectRegionList[regionTypeIndex - 1].id <= 0)) {
      return false;
    }

    this.setData({
      regionType: regionTypeIndex + 1
    })

    let selectRegionItem = selectRegionList[regionTypeIndex];

    this.getRegionList(selectRegionItem.parent_id);

    this.setRegionDoneStatus();

  },
  selectRegion(event) {
    let that = this;
    let regionIndex = event.target.dataset.regionIndex;
    let regionItem = this.data.regionList[regionIndex];
    let regionType = regionItem.type;
    let selectRegionList = this.data.selectRegionList;
    selectRegionList[regionType - 2] = regionItem;


    if (regionType != 4) {
      this.setData({
        selectRegionList: selectRegionList,
        regionType: regionType + 1
      })
      this.getRegionList(regionItem.id);
    } else {
      this.setData({
        selectRegionList: selectRegionList
      })
    }

    //重置下级区域为空
    selectRegionList.map((item, index) => {
      if (index > regionType - 1) {
        item.id = 0;
        item.name = index == 1 ? '城市' : '区县';
        item.parent_id = 0;
      }
      return item;
    });

    this.setData({
      selectRegionList: selectRegionList
    })


    that.setData({
      regionList: that.data.regionList.map(item => {

        //标记已选择的
        if (that.data.regionType == item.type && that.data.selectRegionList[that.data.regionType - 3].id == item.id) {
          item.selected = true;
        } else {
          item.selected = false;
        }

        return item;
      })
    });

    this.setRegionDoneStatus();

  },
  doneSelectRegion() {
    // if (this.data.selectRegionDone === false) {
    //     return false;
    // }

    let address = this.data.address;
    let selectRegionList = this.data.selectRegionList;
    address.province_id = selectRegionList[0].id;
    address.city_id = selectRegionList[1].id;
    address.district_id = selectRegionList[2].id;
    address.province_name = selectRegionList[0].name;
    address.city_name = selectRegionList[1].name;
    address.district_name = selectRegionList[2].name;
    address.full_region = selectRegionList.map(item => {
      return item.name;
    }).join('-');

    this.setData({
      address: address,
      openSelectRegion: false
    });

  },
  cancelSelectRegion() {
    this.setData({
      openSelectRegion: false,
      regionType: this.data.regionDoneStatus ? 3 : 1
    });

  },
  getRegionList(regionId) {
    let that = this;
    let regionType = that.data.regionType;
    util.request(api.RegionList, { parentId: regionId }).then(function (res) {
      if (res.data.errno === 0) {
        that.setData({
          regionList: res.data.data
            .map(item => {

              // 标记已选择的
              if (regionType == item.type && that.data.selectRegionList[regionType - 3].id == item.id) {
                item.selected = true;
              } else {
                item.selected = false;
              }

              return item;
            })
        });
      }
    });
  },
  cancelAddress() {
    wx.navigateTo({
      url: '/pages/ucenter/address/address',
    })
  },
  saveAddress() {
    console.log(this.data.address)
    let address = this.data.address;

    if (address.name == '') {
      util.showErrorToast('请输入姓名');

      return false;
    }

    if (address.mobile == '') {
      util.showErrorToast('请输入手机号码');
      return false;
    }


    if (address.district_id == 0) {
      util.showErrorToast('请输入省市区');
      return false;
    }

    if (address.address == '') {
      util.showErrorToast('请输入详细地址');
      return false;
    }


    let that = this;
    util.request(api.AddressSave, {
      id: address.id,
      name: address.name,
      mobile: address.mobile,
      province_id: address.province_id,
      city_id: address.city_id,
      district_id: address.district_id,
      address: address.address,
      is_default: address.is_default,
    }, 'POST').then(function (res) {
      if (res.errno === 0) {
        wx.navigateTo({
          url: '/pages/ucenter/address/address',
        })
      }
    });

  },
})