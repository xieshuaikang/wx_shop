const ApiRootUrl = 'https://f10.ylwxmall.com/shopping/api/';
// const ApiimgUrl = 'http://f10.ylwxmall.com:6035/photo/';
const ApiimgUrl = 'https://f10.ylwxmall.com/image/';
const ApiUrl = 'https://api.nideshop.com/api/'

module.exports = {
  GET_NOTICE: ApiRootUrl + 'notice/query',  //公告
  apiimgurl: ApiimgUrl,//图片地址
  GET_COMPANY_HOT: ApiRootUrl +'Group/query', //集团自营商品
  IndexUrl: ApiRootUrl + 'Group/query', //首页数据接口
  PRODUCTTYPEONE: ApiRootUrl +'producttype/get_category',//水平获取分类
  PROMOTIONS: ApiRootUrl + 'hotad/query', //首页轮播
   promotquery: ApiRootUrl+ 'promotion/query', //促销查询
  wxLogin: ApiRootUrl+'wx/wxLogin', //微信授权登录
  register:  ApiRootUrl+'/wx/wxRegist',  //注册
  addAddress: ApiRootUrl + '/wx/wxOther',  //添加地址
  sendSms: ApiRootUrl + 'wx/sendMsg/', //微信注册发送验证码
  findAddress: ApiRootUrl +'userInfo/userQuery',// 查询用户详情接口
  codeCheck:ApiRootUrl+'/userRegist/checkCode',  //验证码验证
  getAllGoodsDetailList: ApiRootUrl +'product/list', // 搜索获取商品列表
  productCount: ApiRootUrl + 'product/count',  //统计商品总数
  producttype: ApiRootUrl + 'producttype/get_listcategory', //商品分类接口
  producttypelist: ApiRootUrl +'product/list',
  productDetail: ApiRootUrl + 'product/detail',  //获得商品的详情
  productNew: ApiRootUrl + 'product/new',  //新品
  productHot: ApiRootUrl + 'product/hot',  //热门
  productRelated: ApiRootUrl + 'product/related',  //商品详情页的关联商品（大家都在看）

  BrandList: ApiRootUrl + 'brand/list',  //品牌列表
  BrandDetail: ApiRootUrl + 'brand/detail',  //品牌详情

  CartList: ApiRootUrl + 'cart/getCartList', //获取购物车的列表
  CartAdd: ApiRootUrl + 'cart/setCart', // 添加商品到购物车
  CartUpdate: ApiRootUrl + 'cart/updateCart', // 更新购物车的商品
  CartDelete: ApiRootUrl + 'cart/removeCartList', // 删除购物车的商品
  CartDetail: ApiRootUrl + 'cart/getCart', // 购物车详情


  AddressList: ApiUrl + 'address/list',  //收货地址列表
  AddressDetail: ApiUrl + 'address/detail',  //收货地址详情
  AddressSave: ApiRootUrl + 'address/save',  //保存收货地址
  AddressDelete: ApiRootUrl + 'address/delete',  //保存收货地址
  OrderSubmit: ApiRootUrl + 'order/submitid', // 提交订单
  PayPrepayId: ApiRootUrl + 'pay/prepay', //获取微信统一下单prepay_id

  CommentList: ApiRootUrl + 'comment/list',  //评论列表
  CommentCount: ApiRootUrl + 'comment/count',  //评论总数
  CommentPost: ApiRootUrl + 'comment/post',   //发表评论

  SearchIndex: ApiRootUrl + 'search/index',  //搜索页面数据
  SearchResult: ApiRootUrl + 'search/result',  //搜索数据
  SearchHelper: ApiRootUrl + 'search/helper',  //搜索帮助
  SearchClearHistory: ApiRootUrl + 'search/clearhistory',  //搜索帮助



  RegionList: ApiUrl + 'region/list',  //获取区域列表

  OrderList: ApiRootUrl + 'order/query',  //订单列表
  OrderDetail: ApiRootUrl + 'info/ordernumber',  //订单详情
  OrderCancel: ApiRootUrl + 'order/cancel',  //取消订单
  OrderExpress: ApiRootUrl + 'order/express', //物流详情
  userRegist: ApiRootUrl + 'userRegist/getWayList', 

  FootprintList: ApiRootUrl + 'footprint/list',  //足迹列表
  FootprintDelete: ApiRootUrl + 'footprint/delete',  //删除足迹
};