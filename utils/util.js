var api = require('../config/api.js');

function formatTime(date) {
    var year = date.getFullYear()
    var month = date.getMonth() + 1
    var day = date.getDate()

    var hour = date.getHours()
    var minute = date.getMinutes()
    var second = date.getSeconds()


    return [year, month, day].map(formatNumber).join('-') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

function formatNumber(n) {
    n = n.toString()
    return n[1] ? n : '0' + n
}

function getUserInf () {
    wx.login({
        success: res => {
            // 发送 res.code 到后台换取 openId, sessionKey, unionId
            wxLogin();
            wx.getUserInfo({
              
                success: rs => {
                    // console.log('iv:'+rs.iv);
                    // console.log('encryptedData:'+rs.encryptedData);
                }
            })
        }
    })
}

function userLogin() {
    return new Promise(function (resolve, reject) {
        wx.login({
            success: function () {
                wx.removeStorageSync('sessionid') //必须先清除，否则res.header['Set-Cookie']会报错
                //登录&&创建用户
                request(api.Login).then(function (res) {
                    let sessionid = res.header['Set-Cookie']
                    sessionid = sessionid.match(/=(\S*);/)[1]
                    wx.setStorageSync('sessionid', sessionid)
                })
            },
        })
    })
}

function wxLogin() {
    // 登录
    wx.login({
        success: res => {
            wx.removeStorageSync('sessionid') //必须先清除，否则res.header['Set-Cookie']会报错
            // 发送 res.code 到后台换取 openId, sessionKey, unionId
            wx.getUserInfo({
              
                success: rs => {
                    let code = res.code
                    let iv = rs.iv
                    let encryptedData = (rs.encryptedData)
                    // let code = encodeURIComponent(res.code)
                    // let iv = encodeURIComponent(rs.iv)

                    // let encryptedData = encodeURIComponent(rs.encryptedData)
                    // console.log('iv='+iv+'&'+'code='+code+'&'+'encryptedData='+encryptedData)
                    request(api.wxLogin, {code: code, encryptedData: encryptedData, iv: iv},).then(re => {
                        wx.showToast({
                          title: re.data.msg,
                          icon: 'success',
                          duration: 3000
                        });
                        //登录&&创建用户
                        let sessionid = re.header['Set-Cookie']
                        sessionid = sessionid.match(/=(\S*);/)[1]
                        wx.setStorageSync('sessionid', sessionid)
                    }).catch(err => {
                      wx.showToast({
                        title: re.data.msg,
                        icon: 'none',
                        duration: 3000
                      });
                        console.log('err: ' + res);
                    })
                }
            })

        }

    })

}

/**
 * 封封微信的的request
 */

function request(url, data = {}, method = "GET") {
    var header = {};
    var session_id = wx.getStorageSync('sessionid');//本地取存储的sessionid
    if (session_id != "" && session_id != null) {
        header = {
            'content-type': 'application/json',
            'X-Nideshop-Token': wx.getStorageSync('token'),
            'Cookie': 'JSESSIONID=' + session_id
        }
    }
    return new Promise(function (resolve, reject) {
        wx.request({
            url: url,
            data: data,
            method: method,
            header: header,
            // header: {
            //   'Content-Type': 'application/json',
            //   'X-Nideshop-Token': wx.getStorageSync('token'),
            //     'Cookie': 'JSESSIONID=' + wx.getStorageSync('sessionid')
            // },
            success: function (res) {

                if (res.statusCode == 200) {

                    if (res.data.status == 501) {
                      wx.showToast({
                        title: '请注册正在跳转',
                        icon: 'none',
                        duration: 6000
                      });
                        redirect("/pages/auth/reset/reset")
                        console.log("tiaozhuan");
                    }

                    if (res.data.status == -1) {
                        //需要登录后才可以操作
                      wx.showToast({
                        title: '请登录正在跳转',
                        icon: 'loading',
                        // image:'static/images/icon_error.png',
                        duration: 6000
                      });
                        getUserInf()
                        // let code = null;
                        // return login().then((res) => {
                        //     code = res.code;
                        //     return getUserInfo();
                        // }).then((userInfo) => {
                        //     //登录远程服务器
                        //     request(api.AuthLoginByWeixin, {code: code, userInfo: userInfo}, 'POST').then(res => {
                        //             if (res.errno === 0) {
                        //                 //存储用户信息
                        //                 wx.setStorageSync('userInfo', res.data.userInfo);
                        //                 wx.setStorageSync('token', res.data.token);
                        //                 wx.getUserInfo({})
                        //                     resolve(res);
                        //             } else {
                        //                 reject(res);
                        //             }
                        //         }
                        //     ).catch((err) => {
                        //         reject(err);
                        //     });
                        // }).catch((err) => {
                        //     reject(err);
                        // })
                    } else {
                        resolve(res);
                    }
                } else {
                    reject(res.errMsg);
                }

            },
            fail: function (err) {
                reject(err)
                console.log("failed")
            }
        })
    });
}

/**
 * 检查微信会话是否过期
 */
function checkSession() {
    return new Promise(function (resolve, reject) {
        wx.checkSession({
            success: function () {
                resolve(true);
            },
            fail: function () {
                reject(false);
            }
        })
    });
}

/**
 * 调用微信登录
 */
function login() {
    return new Promise(function (resolve, reject) {
        wx.login({
            success: function (res) {
                if (res.code) {
                    //登录远程服务器
                  wx.showToast({
                    title: res.msg,
                    icon: 'none',
                    duration: 0
                  });
                    console.log(res)
                    resolve(res);
                } else {
                    reject(res);
                }
            },
            fail: function (err) {
                reject(err);
            }
        });
    });
}

function getUserInfo() {
    return new Promise(function (resolve, reject) {
        wx.getUserInfo({
            withCredentials: true,
            success: function (res) {
                console.log(res)
                resolve(res);
            },
            fail: function (err) {
                reject(err);
            }
        })
    });
}

function redirect(url) {

    //判断页面是否需要登录
    if (false) {
        wx.navigateTo({
            url: '/pages/auth/login/login'
        });
        return false;
    } else {
        wx.navigateTo({
            url: url
        });
    }
}

function showErrorToast(msg) {
    wx.showToast({
        title: msg,
        image: '/static/images/icon_error.png'
    })
}

module.exports = {
    formatTime,
    redirect,
    request,
    showErrorToast,
    checkSession,
    login,
    getUserInfo,
    userLogin,
    wxLogin
}


