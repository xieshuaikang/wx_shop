module.exports.Actionsheet = require('./actionsheet/index');
module.exports.Dialog = require('./dialog/index');
module.exports.Field = require('./field/index');
module.exports.NoticeBar = require('./noticebar/index');
module.exports.Select = require('./select/index');
module.exports.Stepper = require('./stepper/index');
module.exports.Switch = require('./switch/index');
module.exports.Tab = require('./tab/index');
module.exports.Toast = require('./toast/index');
module.exports.TopTips = require('./toptips/index');

// 兼容老版本，在下次大版本发布时会被移除
module.exports.CheckLabel = require('./select/index');

const { extend } = require('./common/helper');
module.exports.extend = extend;
